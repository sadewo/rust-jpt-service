use crate::models::{BacklogV2, ProjectBacklog};
use crate::models::{
    //AktivitasInsert,
    AktivitasInsertV2,
    BacklogInsert,
    BacklogUpdate,
    CatatanInsert,
    MemberInsert,
    ProjectInsert,
};
use crate::repositories::*;
use crate::{repositories::backlog, ConnectionPools, DbPool, JSONResult, LoginData};
use actix_web::http::Error;
use actix_web::{web, HttpResponse};
use chrono::{NaiveDate, NaiveDateTime, Utc};
use futures::StreamExt;
use mongodb::bson::oid::ObjectId;
use mongodb::{Client, bson};
use serde::{Deserialize, Serialize};
use std::str::FromStr;
use std::sync::Mutex;
use mongodb::bson::{Bson, doc, from_bson, to_document};
use std::vec;

#[derive(Serialize, Deserialize)]
pub struct BacklogParams {
    nip_lama: Option<String>,
    nama_kerjaan: String,
    skor_urgensi: i32,
    skor_prediktabilitas: i32,
    skp_id: i32,
    sasaran_id: i32,
    urjab_id: i32,
    tgl_mulai: NaiveDate,
    tgl_selesai: Option<NaiveDate>,
    status: Option<String>,
}

#[derive(Deserialize, Serialize, Clone)]
pub struct BacklogParamsV2 {
    id_backlog: Option<String>,
    nama_kerjaan: String,
    skor_urgensi: i32,
    skor_prediktabilitas: i32,
    skp_id: i32,
    sasaran_id: i32,
    urjab_id: i32,
    tgl_mulai: NaiveDate,
    tgl_selesai: Option<NaiveDate>,
}

#[derive(Deserialize, Serialize, Clone)]
pub struct BacklogParamsWithNip {
    nip_lama: Option<String>,
    id_backlog: i32,
    nama_kerjaan: String,
    skor_urgensi: i32,
    skor_prediktabilitas: i32,
    skp_id: i32,
    sasaran_id: i32,
    urjab_id: i32,
    tgl_mulai: NaiveDate,
    tgl_selesai: Option<NaiveDate>,
}

#[derive(Deserialize)]
pub struct AktivitasParams {
    niplama: Option<String>,
    nama_aktivitas: Option<String>,
    key_sort_unit: Option<String>,
    tanggal_aktivitas: NaiveDate,
}

#[derive(Deserialize)]
pub struct BacklogParamsId {
    id: String,
}

#[derive(Serialize)]
struct BacklogResponse<T> {
    success: bool,
    result: T,
}

#[derive(Serialize)]
struct UserorgResponse<T> {
    success: bool,
    result: T,
}

#[derive(Deserialize)]
pub struct UserorgParamsId {
    id: i32,
}

#[derive(Serialize)]
struct AktivitasResponse<T> {
    success: bool,
    result: T,
}

#[derive(Serialize)]
struct SasaranResponse<T> {
    success: bool,
    result: T,
}

#[derive(Serialize)]
struct KinerjaResponse<T> {
    success: bool,
    result: T,
}

#[derive(Serialize)]
struct UrjabResponse<T> {
    success: bool,
    result: T,
}

#[derive(Serialize)]
struct BacklogResult {
    niplama: String,
    // backlog: Vec<BacklogV2>,
    backlog: Vec<BacklogMongoV2>,
}

#[derive(Deserialize)]
pub struct IndikatorParam {
    id_sasaran: Option<i32>,
}

#[derive(Deserialize)]
pub struct NiplamaParam {
    niplama_bawahan: Option<String>,
}

#[derive(Deserialize, Serialize, Clone)]
pub struct ProjectParams {
    judul: String,
    task_list: Option<Vec<BacklogPostParamsV2>>,
    member_list: Option<Vec<MemberParams>>,
    catatan_list: Option<Vec<CatatanParams>>,
    tgl_mulai: NaiveDate,
    tgl_selesai: Option<NaiveDate>,
    deadline: Option<NaiveDate>,
}

#[derive(Deserialize, Serialize, Clone)]
pub struct ProjectMongoV2 {
    judul: String,
    task_list: Vec<BacklogMongoV2>,
    member_list: Option<Vec<MemberParams>>,
    catatan_list: Option<Vec<CatatanParams>>,
    tgl_mulai: NaiveDate,
    tgl_selesai: Option<NaiveDate>,
    deadline: Option<NaiveDate>,
}

#[derive(Deserialize, Serialize, Clone)]
pub struct ProjectDocGet {
    judul: String,
    task_list: Option<Vec<Bson>>,
    member_list: Option<Vec<MemberParams>>,
    catatan_list: Option<Vec<CatatanParams>>,
    tanggal_mulai: NaiveDate,
    tanggal_selesai: Option<NaiveDate>,
    deadline: Option<NaiveDate>,
}

#[derive(Serialize)]
struct ProjectResponse<T> {
    success: bool,
    result: T,
}

#[derive(Deserialize, Serialize, Clone)]
pub struct CatatanParams {
    //id_project: i32,
    catatan: Option<String>,
    link_lampiran: Option<String>,
}

#[derive(Deserialize, Serialize, Clone)]
pub struct CatatanParamsV2 {
    id_project: i32,
    catatan: Option<String>,
    link_lampiran: Option<String>,
}

#[derive(Serialize)]
struct CatatanResponse<T> {
    success: bool,
    result: T,
}

#[derive(Deserialize, Serialize, Clone)]
pub struct MemberParams {
    niplama: String,
    nama: String,
    nip: String,
    unitkerja: String,
    peran: String,
}

#[derive(Serialize)]
struct MemberResponse<T> {
    success: bool,
    result: T,
}

#[derive(Serialize)]
pub struct ProjectId {
    pub project_id: String,
}

#[derive(Serialize)]
struct ProjectResponseV2 {
    pub id: i32,
    pub task_list: Vec<Backlog>,
    pub member_list: Vec<Member>,
    pub judul: String,
    pub tgl_mulai: NaiveDate,
    pub tgl_selesai: Option<NaiveDate>,
    pub deadline: Option<NaiveDate>,
    pub catatan_list: Vec<Catatan>,
}

#[derive(Serialize)]
struct ProjectResponseV3 {
    // pub id: i32,
    pub task_list: Vec<BacklogPostParams>,
    pub member_list: Vec<MemberMongo>,
    pub judul: String,
    pub tgl_mulai: NaiveDate,
    pub tgl_selesai: Option<NaiveDate>,
    pub deadline: Option<NaiveDate>,
    pub catatan_list: Vec<CatatanParams>,
}

#[derive(Serialize)]
pub struct Backlog {
    id: i32,
    nip_lama: Option<String>,
    nama: Option<String>,
    nip: Option<String>,
    nama_kerjaan: Option<String>,
    skor_urgensi: Option<i32>,
    urjab_id: Option<i32>,
    tgl_mulai: Option<NaiveDate>,
    tgl_selesai: Option<NaiveDate>,
    status: Option<i32>,
    total_skor: Option<i32>,
    skp_id: Option<i32>,
    sasaran_id: Option<i32>,
    kode_unit: Option<String>,
    skor_prediktabilitas: Option<i32>,
    id_project: i32,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct BacklogMongo {
    // id: i32,
    #[serde(rename = "_id", skip_serializing_if = "Option::is_none")]
   id: Option<bson::oid::ObjectId>,
    nip_lama: Option<String>,
    nama: Option<String>,
    nip: Option<String>,
    nama_kerjaan: Option<String>,
    skor_urgensi: Option<i32>,
    urjab: UrjabPostParams,
    tgl_mulai: Option<NaiveDate>,
    tgl_selesai: Option<NaiveDate>,
    status: Option<i32>,
    total_skor: Option<i32>,
    skp: SkpPostParams,
    sasaran: SasaranPostParams,
    kode_unit: Option<String>,
    skor_prediktabilitas: Option<i32>,
    // id_project: i32,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct BacklogMongoV2 {
    // id: i32,
    #[serde(rename = "_id", skip_serializing_if = "Option::is_none")]
   id: Option<bson::oid::ObjectId>,
    nip_lama: Option<String>,
    nama: Option<String>,
    nip: Option<String>,
    nama_kerjaan: Option<String>,
    skor_urgensi: Option<i32>,
    urjab_id: i32,
    tgl_mulai: Option<NaiveDate>,
    tgl_selesai: Option<NaiveDate>,
    status: Option<i32>,
    total_skor: Option<i32>,
    skp_id: i32,
    sasaran_id: i32,
    kode_unit: Option<String>,
    skor_prediktabilitas: Option<i32>,
    // id_project: i32,
}
#[derive(Serialize, Deserialize, Clone)]
pub struct BacklogMongoExportV2 {
    nip_lama: Option<String>,
    nama: Option<String>,
    nip: Option<String>,
    nama_kerjaan: Option<String>,
    skor_urgensi: Option<i32>,
    urjab_id: i32,
    tgl_mulai: Option<NaiveDate>,
    tgl_selesai: Option<NaiveDate>,
    status: Option<i32>,
    total_skor: Option<i32>,
    skp_id: i32,
    sasaran_id: i32,
    kode_unit: Option<String>,
    skor_prediktabilitas: Option<i32>,
    // id_project: i32,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct ProjectDocInsert {
    judul: String,
    tanggal_mulai: NaiveDate,
    task_list: Vec<Bson>,
    member_list: Vec<MemberParams>,
    catatan_list: Vec<CatatanParams>,
    tanggal_selesai: Option<NaiveDate>,
    deadline: Option<NaiveDate>,
}

#[derive(Deserialize, Serialize, Clone)]
pub struct BacklogPostParams {
    nip_lama: Option<String>,
    nama: Option<String>,
    nip: Option<String>,
    nama_kerjaan: Option<String>,
    skor_urgensi: Option<i32>,
    urjab: UrjabPostParams,
    tgl_mulai: Option<NaiveDate>,
    tgl_selesai: Option<NaiveDate>,
    status: Option<i32>,
    total_skor: Option<i32>,
    skp: SkpPostParams,
    sasaran: SasaranPostParams,
    kode_unit: Option<String>,
    skor_prediktabilitas: Option<i32>,
}

#[derive(Deserialize, Serialize, Clone)]
pub struct BacklogPostParamsV2 {
    nip_lama: Option<String>,
    nama: Option<String>,
    nip: Option<String>,
    nama_kerjaan: Option<String>,
    skor_urgensi: Option<i32>,
    urjab_id: Option<i32>,
    tgl_mulai: Option<NaiveDate>,
    tgl_selesai: Option<NaiveDate>,
    status: Option<i32>,
    total_skor: Option<i32>,
    skp_id: Option<i32>,
    sasaran_id: Option<i32>,
    kode_unit: Option<String>,
    skor_prediktabilitas: Option<i32>,
}

#[derive(Deserialize, Serialize, Clone)]
pub struct UrjabPostParams {
    id: i32,
    nama_urjab: String,
}

#[derive(Deserialize, Serialize, Clone)]
pub struct SkpPostParams {
    id: i32,
    nama_skp: String,
}

#[derive(Deserialize, Serialize, Clone)]
pub struct SasaranPostParams {
    id: i32,
    nama_sasaran: String,
}

#[derive(Serialize)]
pub struct Member {
    id_project: i32,
    niplama: String,
    nip: String,
    nama: String,
    unitkerja: String,
    peran: String,
}

#[derive(Serialize)]
pub struct MemberMongo {
    // id_project: i32,
    niplama: String,
    nip: String,
    nama: String,
    unitkerja: String,
    peran: String,
}

#[derive(Serialize)]
pub struct Catatan {
    id_project: i32,
    catatan: Option<String>,
    link_lampiran: Option<String>,
}

pub async fn get_backlog(
    pool: web::Data<ConnectionPools>,
    login_data: web::Data<Mutex<LoginData>>,
) -> Result<HttpResponse, Error> {
    let mongo_client = Client::with_uri_str("mongodb://10.10.20.57:27017/")
        .await
        .expect("Koneksi gagal!");
    let db = mongo_client.database("dbhrm");
    let coll = db.collection("backlog_harian_task");
    let niplama = login_data
        .clone()
        .lock()
        .unwrap()
        .user
        .clone()
        .unwrap()
        .user_nip
        .unwrap();
    let pegawai = employee::get_pegawai_by_niplama(&pool.hrm_pool, niplama.clone()).await;
    let bawahan = backlog::get_bawahan_by_kode_jabatan(
        &pool.hrm_pool,
        pegawai.unwrap().s_kd_jabdetail.clone(),
    )
    .await;
    let mut result = vec![];
    println!("{:?}", bawahan);
    if !bawahan.is_empty() {
        for peg in bawahan {
            println!("{:?}", peg.pegawai_id);
            // let backlog = backlog::get_all_backlog_by_niplama(
            //     &pool.hrm_pool,
            //     peg.pegawai_id.clone().unwrap_or("".to_string()),
            // )
            // .await;

            let mut results : Vec<BacklogMongoV2> = vec![];
            let mut docs = coll.find(doc!{"nip_lama": peg.pegawai_id.clone().unwrap_or("".to_string()), "status": 0, "$or": [ {"deleted_at": {"$type": 10}}, {"deleted_at": {"$exists": false}} ]}, None).await.expect("Query gagal!");
                while let Some(doc) = docs.next().await {
                results.push(bson::from_bson(Bson::Document(doc.expect("Dokumen tidak valid!")))
                .expect("Deserialisasi gagal!"))
            }
            // println!("{:?}", backlog);
            if !results.is_empty() {
                result.push(BacklogResult {
                    niplama: peg.pegawai_id.unwrap_or("".to_string()),
                    backlog: results,
                });
            }
        }
    } else {
        // let backlog = backlog::get_all_backlog_by_niplama(&pool.hrm_pool, niplama.clone()).await;
            let mut results : Vec<BacklogMongoV2> = vec![];
            // let mut docs = coll.find(doc!{"nip_lama": niplama.clone(), "status": 0, "deleted_at": {"$type": 10}}, None).await.expect("Query gagal!");
            let mut docs = coll.find(doc!{"nip_lama": niplama.clone(), "status": 0, "$or": [ {"deleted_at": {"$type": 10}}, {"deleted_at": {"$exists": false}} ]}, None).await.expect("Query gagal!");
                while let Some(doc) = docs.next().await {
                results.push(bson::from_bson(Bson::Document(doc.expect("Dokumen tidak valid!")))
                .expect("Deserialisasi gagal!"))
            }
            // println!("{:?}", backlog);
            if !results.is_empty() {
                result.push(BacklogResult {
                    niplama,
                    backlog: results,
                });
            }
    }

    let respons = BacklogResponse {
        success: true,
        result,
    };
    Ok(HttpResponse::Ok().json(respons))
}

pub async fn get_backlog_export(
    pool: web::Data<ConnectionPools>,
    login_data: web::Data<Mutex<LoginData>>,
) -> Result<HttpResponse, Error> {
        let mongo_client = Client::with_uri_str("mongodb://10.10.20.57:27017/")
        .await
        .expect("Koneksi gagal!");
    let db = mongo_client.database("dbhrm");
    let coll = db.collection("backlog_harian_task");
    // let niplama = login_data
    //     .clone()
    //     .lock()
    //     .unwrap()
    //     .user
    //     .clone()
    //     .unwrap()
    //     .user_nip
    //     .unwrap();
    // let pegawai = employee::get_pegawai_by_niplama(&pool.hrm_pool, niplama.clone()).await;
    // let bawahan = backlog::get_bawahan_by_kode_jabatan(
    //     &pool.hrm_pool,
    //     pegawai.unwrap().s_kd_jabdetail.clone(),
    // )
    // .await;
    let mut result = vec![];
    // println!("{:?}", bawahan);
    let backlog = backlog::get_all_backlog(
        &pool.hrm_pool,
    )
    .await;
                for res in backlog {
                // let urjab = backlog::get_uraian_jabatan_by_id(&pool.hrm_pool, res.clone().urjab_id).await;
                // let skp = backlog::get_indikator_by_id(&pool.hrm_pool, res.clone().skp_id).await;
                // let sasaran = backlog::get_sasaran_by_id(&pool.hrm_pool, res.clone().sasaran_id.unwrap_or(0)).await;
                    result.push(BacklogMongoExportV2{
                nip_lama: res.clone().nip_lama,
                nama: res.clone().nama,
                nip: res.clone().nip,
                nama_kerjaan: res.clone().nama_kerjaan,
                skor_urgensi: res.clone().skor_urgensi,
                urjab_id: res.clone().urjab_id.unwrap_or(0),
                tgl_mulai: res.clone().tgl_mulai,
                tgl_selesai: res.clone().tgl_selesai,
                status: res.clone().status,
                total_skor: res.clone().total_skor,
                skp_id: res.clone().skp_id.unwrap_or(0),
                sasaran_id: res.clone().sasaran_id.unwrap_or(0),
                kode_unit: res.clone().kode_unit,
                skor_prediktabilitas: res.clone().skor_prediktabilitas,
                    })
                }

    for res in result {
        let doc= to_document(&res).unwrap();
        let insert_result = coll.insert_one(doc, None).await.expect("Gagal!");
    }

    let respons = JSONResult {
        success: true,
        result: "mantab"
    };
    Ok(HttpResponse::Ok().json(respons))
}

pub async fn get_userorg(pool: web::Data<ConnectionPools>) -> Result<HttpResponse, Error> {
    let result = backlog::get_all_userorg(&pool.hrm_pool).await;
    let respons = UserorgResponse {
        success: true,
        result,
    };
    Ok(HttpResponse::Ok().json(respons))
}

#[derive(Serialize)]
pub struct BawahanList {
    pub niplama: String,
    pub nama: String,
    pub nip: String,
}

pub async fn get_list_bawahan(
    pool: web::Data<ConnectionPools>,
    login_data: web::Data<Mutex<LoginData>>,
) -> Result<HttpResponse, Error> {
    let niplama = login_data
        .clone()
        .lock()
        .unwrap()
        .user
        .clone()
        .unwrap()
        .user_nip
        .unwrap();
    let pegawai = backlog::get_pegawai_by_niplama(&pool.hrm_pool.clone(), niplama.clone()).await;
    let list_bawahan = backlog::get_bawahan_by_kode_jabatan(&pool.hrm_pool.clone(), pegawai).await;
    let mut data_bawahan = vec![];
    for bawahan in list_bawahan {
        let data_pegawai = employee::get_pegawai_by_niplama(
            &pool.hrm_pool.clone(),
            bawahan.pegawai_id.unwrap_or("".to_string()),
        )
        .await;
        if let Some(data_pegawai) = data_pegawai {
            data_bawahan.push(BawahanList {
                niplama: data_pegawai.niplama,
                nama: data_pegawai.nama,
                nip: data_pegawai.nip.unwrap_or("".to_string()),
            });
        }
    }
    let respons = UserorgResponse {
        success: true,
        result: data_bawahan,
    };
    Ok(HttpResponse::Ok().json(respons))
    // }else{
    //     let respons = UserorgResponse{
    //         success: false,
    //         result: "Data pegawai tidak ditemukan!"
    //     };
    //     Ok(HttpResponse::Ok().json(respons))
    // }
}

pub async fn save_backlog(
    pool: web::Data<ConnectionPools>,
    params: web::Json<BacklogPostParamsV2>,
    login_data: web::Data<Mutex<LoginData>>,
) -> Result<HttpResponse, Error> {
    let mut niplama = login_data
        .clone()
        .lock()
        .unwrap()
        .user
        .clone()
        .unwrap()
        .user_nip
        .unwrap();
    let mut kode_unit = "".to_string();
    let nip = params.nip_lama.clone();
    if nip.is_some() {
        niplama = nip.unwrap();
    }
    let mongo_client = Client::with_uri_str("mongodb://10.10.20.57:27017/")
        .await
        .expect("Koneksi gagal!");
    let db = mongo_client.database("dbhrm");
    let coll = db.collection("backlog_harian_task");
    // let ProjectParams { judul, task_list, tgl_mulai, tgl_selesai, deadline } = params.clone();
    // let ProjectParams { judul, task_list, member_list, 
    //     catatan_list, tgl_mulai, tgl_selesai, 
    //     deadline } = params.clone();
    // let BacklogPostParams { nip_lama, nama, nip, nama_kerjaan, 
    //     skor_urgensi, urjab, tgl_mulai, tgl_selesai, 
    //     status, total_skor, skp, sasaran, kode_unit, 
    //     skor_prediktabilitas } = params.clone();
    let BacklogPostParamsV2 { nip_lama, nama, nip, nama_kerjaan, skor_urgensi, urjab_id, tgl_mulai, tgl_selesai, status, total_skor, skp_id, sasaran_id, kode_unit, skor_prediktabilitas } = params.clone();
    let backlog = BacklogPostParamsV2 {
        nip_lama,
        nama,
        nip,
        nama_kerjaan,
        skor_urgensi,
        urjab_id,
        tgl_mulai,
        tgl_selesai,
        status,
        total_skor,
        skp_id,
        sasaran_id,
        kode_unit,
        skor_prediktabilitas,
    };

    let doc= to_document(&backlog).unwrap();

    let insert_result = coll.insert_one(doc, None).await.expect("Gagal!");

    let result = coll.find_one(doc!{"_id": insert_result.inserted_id}, None).await.expect("Find gagal!");

    // let data_pegawai =
    //     employee::get_pegawai_by_niplama(&pool.hrm_pool.clone(), niplama.clone()).await;
    // if data_pegawai.is_some() {
    //     kode_unit = data_pegawai
    //         .unwrap()
    //         .key_sort_unit
    //         .unwrap_or("".to_string());
    // }

    // let request = BacklogInsert {
    //     nip_lama: Some(niplama),
    //     nama_kerjaan: params.nama_kerjaan.clone(),
    //     skor_urgensi: params.skor_urgensi,
    //     urjab_id: params.urjab_id,
    //     tgl_mulai: params.tgl_mulai,
    //     tgl_selesai: params.tgl_selesai,
    //     status: Some(0),
    //     total_skor: params.skor_prediktabilitas * params.skor_urgensi,
    //     skp_id: params.skp_id,
    //     kode_unit,
    //     sasaran_id: params.sasaran_id,
    //     skor_prediktabilitas: params.skor_prediktabilitas,
    // };

    // let result =
    //     backlog::post_backlog(&pool.hrm_pool.clone(), request, params.nip_lama.clone()).await;

    let response = BacklogResponse {
        success: true,
        result,
    };
    Ok(HttpResponse::Ok().json(response))
}

#[derive(Serialize, Deserialize, Clone)]
struct BacklogEdit {
    nama_kerjaan: String,
    skor_urgensi: i32,
    skor_prediktabilitas: i32,
    skp_id: i32,
    sasaran_id: i32,
    urjab_id: i32,
    tgl_mulai: NaiveDate,
    tgl_selesai: Option<NaiveDate>,
}

pub async fn get_backlog_update(
    pool: web::Data<ConnectionPools>,
    params: web::Json<BacklogParamsV2>,
) -> Result<HttpResponse, Error> {
    // let request = BacklogUpdate {
    //     // nip_lama: params.nip_lama.clone(),
    //     nama_kerjaan: params.nama_kerjaan.clone(),
    //     skor_urgensi: params.skor_urgensi,
    //     urjab_id: params.urjab_id,
    //     tgl_mulai: params.tgl_mulai,
    //     tgl_selesai: params.tgl_selesai,
    //     // status: params.status.clone(),
    //     total_skor: params.skor_prediktabilitas * params.skor_urgensi,
    //     skp_id: params.skp_id,
    //     sasaran_id: params.sasaran_id,
    //     skor_prediktabilitas: params.skor_prediktabilitas,
    // };
    let BacklogParamsV2 { id_backlog, nama_kerjaan, skor_urgensi, skor_prediktabilitas, skp_id, 
        sasaran_id, urjab_id, tgl_mulai, tgl_selesai } = params.clone();
    let backlog_edit = BacklogEdit{ nama_kerjaan, skor_urgensi, skor_prediktabilitas, skp_id, sasaran_id, 
        urjab_id, tgl_mulai, tgl_selesai };
    let edit_doc = to_document(&backlog_edit).expect("Convert document gagal!");
    let mongo_client = Client::with_uri_str("mongodb://10.10.20.57:27017/")
    .await
    .expect("Koneksi gagal!");
    let db = mongo_client.database("dbhrm");
    let coll = db.collection("backlog_harian_task");
    let result = coll.find_one_and_update(doc!{"_id": ObjectId::from_str(&id_backlog.unwrap()).expect("generate object ID gagal!")},
     doc!{
         "$set": edit_doc
     }, None).await.unwrap();

    // let result =
    //     backlog::post_backlog_update_v2(&pool.hrm_pool.clone(), request, params.id_backlog.unwrap()).await;

    let response = BacklogResponse {
        success: true,
        result,
    };
    Ok(HttpResponse::Ok().json(response))
}

pub async fn get_backlog_delete(
    pool: web::Data<ConnectionPools>,
    params: web::Json<BacklogParamsId>,
) -> Result<HttpResponse, Error> {
    // let result = backlog::post_backlog_delete_v2(&pool.hrm_pool.clone(), params.id).await;
    // let result = "";
    let mongo_client = Client::with_uri_str("mongodb://10.10.20.57:27017/")
        .await
        .expect("Koneksi gagal!");
    let db = mongo_client.database("dbhrm");
    let coll = db.collection("backlog_harian_task");
    let result = coll.find_one_and_update(doc!{"_id": ObjectId::with_string(&params.id.clone()).unwrap()},
     doc!{"$currentDate": {"lastModified": true, "deleted_at": {"$type": "timestamp"}}}, None).await.expect("Update failed!");

    let response = BacklogResponse {
        success: true,
        result,
    };
    Ok(HttpResponse::Ok().json(response))
}

pub async fn get_backlog_done(
    pool: web::Data<ConnectionPools>,
    params: web::Json<BacklogParamsId>,
    //paramsId: web::Json<BacklogParamsId>,
) -> Result<HttpResponse, Error> {
    let mongo_client = Client::with_uri_str("mongodb://10.10.20.57:27017/")
        .await
        .expect("Koneksi gagal!");
    let db = mongo_client.database("dbhrm");
    let coll = db.collection("backlog_harian_task");
    let result = coll.find_one_and_update(doc!{"_id": ObjectId::with_string(&params.id.clone()).unwrap()},
     doc!{"$set": {"status": 1}}, None).await.expect("Update failed!");
    // let backlog = backlog::get_backlog_by_id(&pool.hrm_pool.clone(), params.id).await;
    // if let Some(backlog) = backlog {
    //     let request = AktivitasInsertV2 {
    //         niplama: backlog.nip_lama.clone(),
    //         nama_aktivitas: backlog.nama_kerjaan.clone(),
    //         key_sort_unit: backlog.kode_unit.clone(),
    //         tanggal_aktivitas: Utc::now().naive_local().date(),
    //         id_b: Some(params.id),
    //         id_sasaran: backlog.sasaran_id.unwrap_or(0),
    //         id_sub_sasaran: backlog.skp_id.unwrap_or(0),
    //     };

    //     let result = backlog::post_aktivitas(
    //         &pool.hrm_pool.clone(),
    //         request,
    //         // paramsId.id_b,
    //     )
    //     .await;

    //     let response = AktivitasResponse {
    //         success: true,
    //         result,
    //     };
    //     Ok(HttpResponse::Ok().json(response))
    // } else {
    //     let response = AktivitasResponse {
    //         success: false,
    //         result: "Data backlog tidak ditemukan",
    //     };
    //     Ok(HttpResponse::Ok().json(response))
    // }
    let response = AktivitasResponse {
        success: true,
        result,
    };
    Ok(HttpResponse::Ok().json(response))
}

pub async fn get_sasaran(pool: web::Data<ConnectionPools>) -> Result<HttpResponse, Error> {
    let result = backlog::get_all_sasaran(&pool.hrm_pool).await;
    let respons = SasaranResponse {
        success: true,
        result,
    };
    Ok(HttpResponse::Ok().json(respons))
}

pub async fn get_indikator(
    pool: web::Data<ConnectionPools>,
    param: web::Query<IndikatorParam>,
) -> Result<HttpResponse, Error> {
    let result = backlog::get_all_indikator(&pool.hrm_pool, param.id_sasaran).await;
    let respons = KinerjaResponse {
        success: true,
        result,
    };
    Ok(HttpResponse::Ok().json(respons))
}

pub async fn get_uraian_jabatan(
    pool: web::Data<ConnectionPools>,
    login_data: web::Data<Mutex<LoginData>>,
    niplama_param: web::Query<NiplamaParam>,
) -> Result<HttpResponse, Error> {
    let mut niplama = login_data
        .clone()
        .lock()
        .unwrap()
        .user
        .clone()
        .unwrap()
        .user_nip
        .unwrap();
    let niplama_bawahan = niplama_param.niplama_bawahan.clone();
    if niplama_bawahan.is_some() {
        niplama = niplama_bawahan.unwrap()
    }
    let pegawai = backlog::get_pegawai_by_niplama(&pool.hrm_pool, niplama.clone()).await;
    let result = backlog::get_uraian_jabatan_by_kode(&pool.hrm_pool, pegawai.clone()).await;
    let respons = UrjabResponse {
        success: true,
        result,
    };
    Ok(HttpResponse::Ok().json(respons))
}

pub async fn get_project(pool: web::Data<ConnectionPools>) -> Result<HttpResponse, Error> {
    let mongo_client = Client::with_uri_str("mongodb://10.10.20.57:27017/")
        .await
        .expect("Koneksi gagal!");
    let db = mongo_client.database("dbhrm");
    let coll = db.collection("backlog_harian_project");
    let task_coll = db.collection("backlog_harian_task");
    let mut project_results : Vec<ProjectDocGet> = vec![];
    let mut docs = coll.find(None, None).await.expect("Query gagal!");
            while let Some(doc) = docs.next().await {
            project_results.push(bson::from_bson(Bson::Document(doc.expect("Dokumen tidak valid!")))
            .expect("Deserialisasi gagal!"))
        }

    let mut results = vec![];
    for res in project_results {
        let ProjectDocGet { judul, task_list, member_list, 
            catatan_list, tanggal_mulai, 
            tanggal_selesai, deadline } = res.clone();
        let mut task_results : Vec<BacklogMongoV2> = vec![];
        for task in task_list.unwrap_or(vec![]) {
            let mut task_docs = task_coll.find(doc!{"_id": task}, None).await.expect("Query gagal!");
                    while let Some(doc) = task_docs.next().await {
                    task_results.push(bson::from_bson(Bson::Document(doc.expect("Dokumen tidak valid!")))
                    .expect("Deserialisasi gagal!"))
                }
        }
        results.push(ProjectMongoV2{
            judul,
            task_list: task_results,
            member_list,
            catatan_list,
            tgl_mulai: tanggal_mulai,
            tgl_selesai: tanggal_selesai,
            deadline,
        })

    }
    // let project_result = backlog::get_all_project(&pool.hrm_pool).await;
    // let mut results = Vec::new();
    // for pro in project_result.iter() {
    //     let backlog_project = backlog::get_backlog_by_project(&pool.hrm_pool, pro.clone().id).await;
    //     let catatan_project = backlog::get_catatan_by_project(&pool.hrm_pool, pro.clone().id).await;
    //     let member_project = backlog::get_member_by_project(&pool.hrm_pool, pro.clone().id).await;

    //     let mut member_data = Vec::new();
    //     for mem in member_project.iter() {
    //         let mut member = Member {
    //             id_project: mem.clone().id_project,
    //             niplama: mem.clone().niplama,
    //             nip: mem.clone().nip,
    //             nama: mem.clone().nama,
    //             unitkerja: mem.clone().unitkerja,
    //             peran: mem.clone().peran,
    //         };
    //         member_data.push(member);
    //     }

    //     let mut catatan_data = Vec::new();
    //     for cat in catatan_project.iter() {
    //         let mut catatan = Catatan {
    //             id_project: cat.clone().id_project,
    //             catatan: cat.clone().catatan,
    //             link_lampiran: cat.clone().link_lampiran,
    //         };
    //         catatan_data.push(catatan);
    //     }

    //     let mut backlog_data = Vec::new();
    //     for bac in backlog_project.iter() {
    //         let mut backlog = Backlog {
    //             id: bac.clone().id,
    //             nip_lama: bac.clone().nip_lama,
    //             nama: bac.clone().nama,
    //             nip: bac.clone().nip,
    //             nama_kerjaan: bac.clone().nama_kerjaan,
    //             skor_urgensi: bac.clone().skor_urgensi,
    //             urjab_id: bac.clone().urjab_id,
    //             tgl_mulai: bac.clone().tgl_mulai,
    //             tgl_selesai: bac.clone().tgl_selesai,
    //             status: bac.clone().status,
    //             total_skor: bac.clone().total_skor,
    //             skp_id: bac.clone().skp_id,
    //             sasaran_id: bac.clone().sasaran_id,
    //             kode_unit: bac.clone().kode_unit,
    //             skor_prediktabilitas: bac.clone().skor_prediktabilitas,
    //             id_project: bac.clone().id_project,
    //         };
    //         backlog_data.push(backlog);
    //     }

    //     results.push(ProjectResponseV2 {
    //         id: pro.clone().id,
    //         task_list: backlog_data,
    //         member_list: member_data,
    //         judul: pro.clone().judul,
    //         tgl_mulai: pro.clone().tgl_mulai,
    //         tgl_selesai: pro.clone().tgl_selesai,
    //         deadline: pro.clone().deadline,
    //         catatan_list: catatan_data,
    //     })
    // }

    let respons = ProjectResponse {
        success: true,
        result: results,
    };
    Ok(HttpResponse::Ok().json(respons))
}

pub async fn import_project(pool: web::Data<ConnectionPools>) -> Result<HttpResponse, Error> {
    let mongo_client = Client::with_uri_str("mongodb://10.10.20.57:27017/")
        .await
        .expect("Koneksi gagal!");
    let db = mongo_client.database("dbhrm");
    let coll = db.collection("backlog_harian_project");
    let mut results : Vec<ProjectParams> = vec![];
    let mut docs = coll.find(None, None).await.expect("Query gagal!");
            while let Some(doc) = docs.next().await {
            results.push(bson::from_bson(Bson::Document(doc.expect("Dokumen tidak valid!")))
            .expect("Deserialisasi gagal!"))
        }
    let project_result = backlog::get_all_project(&pool.hrm_pool).await;
    let mut results = Vec::new();
    for pro in project_result.iter() {
        let backlog_project = backlog::get_backlog_by_project(&pool.hrm_pool, pro.clone().id).await;
        let catatan_project = backlog::get_catatan_by_project(&pool.hrm_pool, pro.clone().id).await;
        let member_project = backlog::get_member_by_project(&pool.hrm_pool, pro.clone().id).await;

        let mut member_data = Vec::new();
        for mem in member_project.iter() {
            let mut member = MemberMongo {
                // id_project: mem.clone().id_project,
                niplama: mem.clone().niplama,
                nip: mem.clone().nip,
                nama: mem.clone().nama,
                unitkerja: mem.clone().unitkerja,
                peran: mem.clone().peran,
            };
            member_data.push(member);
        }

        let mut catatan_data = Vec::new();
        for cat in catatan_project.iter() {
            let mut catatan = CatatanParams {
                // id_project: cat.clone().id_project,
                catatan: cat.clone().catatan,
                link_lampiran: cat.clone().link_lampiran,
            };
            catatan_data.push(catatan);
        }

        let mut backlog_data = Vec::new();
        for bac in backlog_project.iter() {
            let urjab = backlog::get_uraian_jabatan_by_id(&pool.hrm_pool, bac.clone().urjab_id).await;
            let skp = backlog::get_indikator_by_id(&pool.hrm_pool, bac.clone().skp_id).await;
            let sasaran = backlog::get_sasaran_by_id(&pool.hrm_pool, bac.clone().sasaran_id.unwrap_or(0)).await;
            let mut backlog = BacklogPostParams {
                // id: bac.clone().id,
                nip_lama: bac.clone().nip_lama,
                nama: bac.clone().nama,
                nip: bac.clone().nip,
                nama_kerjaan: bac.clone().nama_kerjaan,
                skor_urgensi: bac.clone().skor_urgensi,
                urjab: UrjabPostParams{
                    id: urjab.clone().unwrap().id,
                    nama_urjab: urjab.unwrap().nama_sasaran.unwrap_or("".to_string()),
                },
                tgl_mulai: bac.clone().tgl_mulai,
                tgl_selesai: bac.clone().tgl_selesai,
                status: bac.clone().status,
                total_skor: bac.clone().total_skor,
                skp: SkpPostParams{
                    id: skp.clone().unwrap().id_sub_sasaran,
                    nama_skp: skp.unwrap().nama_indikator.unwrap_or("".to_string()),
                },
                sasaran: SasaranPostParams{
                    id: sasaran.clone().unwrap().id_sasaran,
                    nama_sasaran: sasaran.unwrap().nama_sasaran_kinerja.unwrap_or("".to_string()),
                },
                kode_unit: bac.clone().kode_unit,
                skor_prediktabilitas: bac.clone().skor_prediktabilitas,
                // id_project: bac.clone().id_project,
            };
            backlog_data.push(backlog);
        }

        results.push(ProjectResponseV3 {
            // id: pro.clone().id,
            task_list: backlog_data,
            member_list: member_data,
            judul: pro.clone().judul,
            tgl_mulai: pro.clone().tgl_mulai,
            tgl_selesai: pro.clone().tgl_selesai,
            deadline: pro.clone().deadline,
            catatan_list: catatan_data,
        })
    }

    for result in results {
        let doc= to_document(&result).unwrap();
        let insert_result = coll.insert_one(doc, None).await.expect("Gagal!");
    }

    let respons = ProjectResponse {
        success: true,
        result: "mantab",
    };
    Ok(HttpResponse::Ok().json(respons))
}

#[derive(Serialize, Deserialize, Clone)]
struct ProjectIdInsert {
    project_id: String
}

pub async fn save_project(
    pool: web::Data<ConnectionPools>,
    params: web::Json<ProjectParams>,
    //login_data: web::Data<Mutex<LoginData>>
) -> Result<HttpResponse, Error> {
    //let mut niplama = login_data.clone().lock().wrunap().user.clone().unwrap().user_nip.unwrap();
    // let mut kode_unit = "".to_string();
    // let nip = params.nip_lama.clone();
    // if nip.is_some(){
    //     niplama = nip.unwrap();
    // }
    // let data_pegawai = employee::get_pegawai_by_niplama(&pool.hrm_pool.clone(), niplama.clone()).await;
    // if data_pegawai.is_some(){
    //     kode_unit = data_pegawai.unwrap().key_sort_unit.unwrap_or("".to_string());
    // }
    let mongo_client = Client::with_uri_str("mongodb://10.10.20.57:27017/")
        .await
        .expect("Koneksi gagal!");
    let db = mongo_client.database("dbhrm");
    let coll = db.collection("backlog_harian_project");
    let task_coll = db.collection("backlog_harian_task");
    // let ProjectParams { judul, task_list, tgl_mulai, tgl_selesai, deadline } = params.clone();
    let ProjectParams { judul, task_list, member_list, 
        catatan_list, tgl_mulai, tgl_selesai, 
        deadline } = params.clone();
    // let project = ProjectParams {
    //     judul,
    //     task_list,
    //     tgl_mulai,
    //     tgl_selesai,
    //     deadline,
    //     member_list,
    //     catatan_list,
    // };
    let mut backlog_id_list = vec![];
    if let Some(task_list) = task_list {
        for task in task_list {
            let BacklogPostParamsV2 { nip_lama, nama, nip, 
                nama_kerjaan, skor_urgensi, urjab_id, 
                tgl_mulai, tgl_selesai, status, 
                total_skor, skp_id, sasaran_id, kode_unit, 
                skor_prediktabilitas } = task.clone();
            let backlog_insert_doc = to_document(&BacklogPostParamsV2{
                nip_lama,
                nama,
                nip,
                nama_kerjaan,
                skor_urgensi,
                urjab_id,
                tgl_mulai,
                tgl_selesai,
                status,
                total_skor,
                skp_id,
                sasaran_id,
                kode_unit,
                skor_prediktabilitas,
            }).unwrap();
            let new_task_result = task_coll.insert_one(backlog_insert_doc, None).await.expect("Gagal!");
            backlog_id_list.push(new_task_result.inserted_id);
        }
    }

    let project_doc = ProjectDocInsert{ 
        judul, 
        tanggal_mulai: tgl_mulai, 
        tanggal_selesai: tgl_selesai, 
        task_list: backlog_id_list.clone(),
        member_list: member_list.unwrap_or(vec![]),
        catatan_list: catatan_list.unwrap_or(vec![]),
        deadline
    };

    println!("{:?}", backlog_id_list.clone());

    let doc= to_document(&project_doc).unwrap();
    let insert_result = coll.insert_one(doc, None).await.expect("Gagal!");

    let project_id_insert = to_document(&ProjectIdInsert{ project_id: insert_result.inserted_id.to_string() }).expect("Gagal generate docs!");
    for backlog_id in backlog_id_list {
        let backlog_update_result = task_coll.find_one_and_update(doc!{"_id": backlog_id }, doc!{"$set": project_id_insert.clone()}, None).await.expect("Update gagal!");
    }

    let result = coll.find_one(doc!{"_id": insert_result.inserted_id}, None).await.expect("Find gagal!");
    // let request = ProjectInsert {
    //     //nip_lama: Some(niplama),
    //     judul: params.judul.clone(),
    //     tgl_mulai: params.tgl_mulai,
    //     tgl_selesai: params.tgl_selesai,
    //     deadline: params.deadline,
    // };

    // let result = backlog::post_project(
    //     &pool.hrm_pool.clone(),
    //     request,
    //     // params.nip_lama.clone(),
    // )
    // .await;

    // let result_cat= backlog::post_catatan(
    //     &pool.hrm_pool.clone(),
    //     request_cat,
    // ).await;

    // let result_mem= backlog::post_member(
    //     &pool.hrm_pool.clone(),
    //     request_mem,
    // ).await;


    let response = ProjectResponse {
        success: true,
        result,
    };
    Ok(HttpResponse::Ok().json(response))
}

pub async fn save_catatan(
    pool: web::Data<ConnectionPools>,
    params: web::Json<CatatanParamsV2>,
)-> Result<HttpResponse, Error> {
    let request = CatatanInsert{
        id_project : params.id_project,
        catatan: params.catatan.clone(),
        link_lampiran: params.link_lampiran.clone(),
    };

    let result = backlog::post_catatan(
        &pool.hrm_pool.clone(),
        request,
    ).await;

    let response = CatatanResponse {
        success: true,
        result,
    };
    Ok(HttpResponse::Ok().json(response))
}

pub async fn save_member(
    pool: web::Data<ConnectionPools>,
    params: web::Json<MemberParams>,

)-> Result<HttpResponse, Error> {
    let niplama = params.niplama.clone();
    let data_pegawai = employee::get_pegawai_by_niplama(&pool.hrm_pool.clone(), niplama.clone()).await;
    
    let request = MemberInsert{
        id_project : 1,
        niplama: niplama.clone(),
        nip: data_pegawai.clone().unwrap().nip.unwrap_or("".to_string()),
        nama: data_pegawai
            .clone()
            .unwrap()
            .s_nama_lengkap
            .unwrap_or("".to_string()),
        unitkerja: data_pegawai
            .clone()
            .unwrap()
            .namaunit_lengkap
            .unwrap_or("".to_string()),
        peran: data_pegawai
            .clone()
            .unwrap()
            .namaunit_lengkap
            .unwrap_or("".to_string()),
    };

    let result = backlog::post_member(
        &pool.hrm_pool.clone(),
        request,
    ).await;
    let response = MemberResponse {
        success: true,
        result,
    };
    Ok(HttpResponse::Ok().json(response))
}